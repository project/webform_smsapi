<?php

declare(strict_types=1);

namespace Drupal\webform_smsapi\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\smsapi\Services\SmsapiServiceInterface;
use Drupal\smsapi\Services\SmsapiSmsTemplateServiceInterface;
use Drupal\smsapi\SmsapiSmsTemplateInterface;
use Drupal\webform\Element\WebformAjaxElementTrait;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send SMS message about webform submission.
 *
 * @WebformHandler(
 *   id = "smsapi",
 *   label = @Translation("SMS Message"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Sends a SMS Message."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class SmsApiWebformHandler extends WebformHandlerBase {

  use WebformAjaxElementTrait;

  /**
   * The Webform element plugin manager.
   */
  protected WebformElementManagerInterface $elementManager;

  /**
   * The SMSAPI Service.
   */
  protected SmsapiServiceInterface $smsapiService;

  /**
   * The SMSAPI SMS Template Service.
   */
  protected SmsapiSmsTemplateServiceInterface $templateService;

  /**
   * Possible phone number fields from Webform.
   */
  private array $phoneElementOptions = [];

  /**
   * Possible select list number fields from Webform.
   */
  private array $optionsElementOptions = [];

  /**
   * Available SMS senders.
   */
  private array $senderOptions = [];

  /**
   * Available SMSAPI SMS Templates.
   */
  private array $smsTemplateOptions = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->elementManager = $container->get('plugin.manager.webform.element');
    $instance->smsapiService = $container->get('smsapi.service');
    $instance->templateService = $container->get('smsapi.template_service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary(): array {
    $settings = $this->configuration;
    array_walk_recursive($settings, function (&$value): void {
      $value = $this->trimWebformTokens($value);
    });
    return [
      '#settings' => $settings,
    ] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'to' => [],
      'from' => [],
      'message' => [],
      'template' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $this->loadRecipientOptions();
    $this->loadSenderOptions();
    $this->loadTemplateOptions();

    $defaultOptions = [
      '' => $this->t('- Select -')->__toString(),
      'custom' => $this->t('Custom phone number')->__toString(),
      $this->t('Elements')->__toString() => $this->phoneElementOptions,
    ];

    if (!empty($this->optionsElementOptions)) {
      $defaultOptions[$this->t('Options')->__toString()] = $this->optionsElementOptions;
    }

    // Send to form element.
    $form['to'] = [
      '#type' => 'details',
      '#title' => $this->t('Send to'),
      '#open' => TRUE,
      '#help' => $this->t("Phone number/s of message recipient/s."),
      'to_number' => [
        '#type' => 'select',
        '#title' => $this->t('To phone number'),
        '#options' => $defaultOptions,
        '#description' => $this->t('Each phone number must be seperated by comma'),
        '#default_value' => $this->configuration['to']['to_number'] ?? '',
      ],
      'custom_number' => [
        '#type' => 'textfield',
        '#title' => $this->t('Custom phone number/s'),
        '#default_value' => $this->configuration['to']['custom_number'] ?? '',
        '#description' => $this->t("For more recipients, separate each number with a comma"),
        '#states' => [
          'visible' => [
            ':input[name="settings[to][to_number]"]' => ['value' => 'custom'],
          ],
        ],
      ],
    ];

    // Send from form element.
    $form['from'] = [
      '#type' => 'details',
      '#title' => $this->t('Send from'),
      '#open' => TRUE,
      '#help' => $this->t("Select SMS sender name defined in SMSApi dashboard."),
    ];

    if (empty($this->senderOptions)) {
      $form['from']['missing_sender'] = [
        '#type' => 'markup',
        '#markup' => $this->t('Missing / Wrong SMSApi token.<br/> Check <a href="@url">configuration</a>',
          [
            '@url' => Url::fromRoute('smsapi.configuration')->toString(),
          ]),
      ];
    }
    else {
      $form['from']['from_sender'] = [
        '#type' => 'select',
        '#title' => $this->t('Sender'),
        '#options' => $this->senderOptions,
        '#default_value' => $this->configuration['from']['from_sender'] ?? '',
      ];
    }

    // Message form element.
    $form['message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message'),
      '#open' => TRUE,
    ];

    $messageOptions = [
      '' => $this->t('- Select -')->__toString(),
      '_custom' => $this->t('Custom text message')->__toString(),
    ];
    if (!empty($this->smsTemplateOptions)) {
      $messageOptions[$this->t('SMS Templates')->__toString()] = $this->smsTemplateOptions;
    }

    $form['message']['message_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of message'),
      '#options' => $messageOptions,
      '#default_value' => $this->configuration['message']['message_type'] ?? NULL,
      '#ajax' => [
        'callback' => [$this, 'loadTokenFields'],
        'event' => 'change',
        'wrapper' => 'sms_template_wrapper',
      ],
    ];
    $form['message']['info'] = [
      '#type' => 'item',
      '#markup' => $this->t('<small>In order to configure SMS Templates click <a href="@url">here</a></small>',
        [
          '@url' => Url::fromRoute('entity.smsapi_sms_template.collection')->toString(),
        ]),
      '#states' => [
        'visible' => [
          ':input[name="settings[message][message_type]"]' => ['filled' => FALSE],
        ],
      ],
    ];
    $form['message']['custom_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom SMS Message'),
      '#default_value' => $this->configuration['message']['custom_message'] ?? '',
      '#states' => [
        'visible' => [
          ':input[name="settings[message][message_type]"]' => ['value' => '_custom'],
        ],
      ],
    ];
    $form['message']['sms_template'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'sms_template_wrapper',
      ],
    ];

    if ($form_state->isRebuilding()) {
      $template = $form_state->getValue(['message', 'message_type']);
    }
    else {
      $template = $this->configuration['template'] ?? '';
    }

    // Generate template fields token replacement.
    $template = is_string($template) ? $template : '';
    if (!empty($template) && $template != '_custom') {
      $this->generateTemplateFields($template, $defaultOptions, $form);
    }

    return $form;
  }

  /**
   * Ajax callback to load token fields.
   */
  public function loadTokenFields(array $form, FormStateInterface $form_state): array {
    return $form['settings']['message']['sms_template'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);
    $values = $form_state->getValues();

    // Validate no phone number.
    if (empty($values['to']['to_number'])) {
      $form_state->setError(
        $form['settings']['to'],
        $this->t('Phone number is required.'),
      );
    }

    // Validate custom phone number.
    if (
        isset($values['to']['to_number'])
        && $values['to']['to_number'] == 'custom'
        && empty($values['to']['custom_number'])
    ) {
      $form_state->setError(
        $form['settings']['to']['custom_number'],
        $this->t('Custom phone number is required.'),
      );
    }

    // Validate no sender field.
    if (isset($values['from']['missing_sender'])) {
      $form_state->setError(
        $form['settings']['from'],
        $this->t('Missing / Wrong SMSApi token. Check configuration.'),
      );
    }

    // Validate empty sender.
    if (empty($values['from']['from_sender'])) {
      $errorField = $form['settings']['from']['from_sender'] ?? $form['settings']['from'];
      $form_state->setError(
        $errorField,
        $this->t('SMS sender is required.'),
      );
    }

    // Validate empty message.
    if (empty($values['message']['message_type'])) {
      $errorField = $form['settings']['message'] ?? $form['settings'];
      $form_state->setError(
        $errorField,
        $this->t('SMS Message is required.'),
      );
    }

    // Validate empty custom message.
    if (
        isset($values['message']['message_type'])
        && $values['message']['message_type'] == '_custom'
        && empty($values['message']['custom_message'])
    ) {
      $form_state->setError(
        $form['settings']['message']['custom_message'],
        $this->t('Custom SMS message is required.'),
      );
    }

    // Validate template tokens.
    if (isset($values['message']['message_type']) && $values['message']['message_type'] != '_custom') {
      $tokenValues = $values['message']['sms_template']['tokens'] ?? [];

      foreach ($tokenValues as $token => $tokenData) {

        // Validate empty token value.
        if (empty($tokenData['type'])) {
          $form_state->setError(
            $form['settings']['message']['sms_template']['tokens'][$token]['type'],
            $this->t('Value for @token is required.', ['@token' => $token]),
          );
        }

        // Validate custom token value.
        if (isset($tokenData['type']) && $tokenData['type'] == 'custom' && empty($tokenData['custom_token'])) {
          $form_state->setError(
            $form['settings']['message']['sms_template']['tokens'][$token]['custom_token'],
            $this->t('Custom value for @token is required.', [
              '@token' => $token,
            ]),
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE): void {
    $state = (boolean) $webform_submission->getWebform()->getSetting('results_disabled') ? WebformSubmissionInterface::STATE_COMPLETED : $webform_submission->getState();
    if ($state == WebformSubmissionInterface::STATE_COMPLETED) {
      $smsData = $this->parseConfigData($webform_submission, $this->configuration);
      $this->sendSms($smsData);
    }
  }

  /**
   * Load available recipient values for sending sms.
   */
  private function loadRecipientOptions(): void {
    $phoneElementOptions = [];
    $optionsElementOptions = [];
    $elements = $this->webform->getElementsInitializedAndFlattened();
    foreach ($elements as $element_key => $element) {
      $element_plugin = $this->elementManager->getElementInstance($element);
      if (!$element_plugin->isInput($element) || !isset($element['#type'])) {
        continue;
      }

      // Set title.
      $element_title = (isset($element['#title'])) ? (string) $this->t('@title (@key)', [
        '@title' => $element['#title'],
        '@key' => $element_key,
      ]) : $element_key;

      // Add options element token, which can include multiple values.
      if (isset($element['#options'])) {
        $optionsElementOptions["[webform_submission:values:$element_key:raw]"] = $element_title;
      }

      // Multiple value elements can NOT be used as a tokens.
      if ($element_plugin->hasMultipleValues($element)) {
        continue;
      }

      if (!$element_plugin->isComposite()) {
        if (in_array($element['#type'], ['tel', 'hidden', 'value', 'textfield'], TRUE)) {
          $phoneElementOptions["[webform_submission:values:$element_key:raw]"] = $element_title;
        }
      }

      // Handle composite sub elements.
      if ($element_plugin instanceof WebformCompositeBase) {
        $composite_elements = $element_plugin->getCompositeElements();
        foreach ($composite_elements as $composite_key => $composite_element) {
          $composite_element_plugin = $this->elementManager->getElementInstance($element);
          if (!$composite_element_plugin->isInput($element) || !isset($composite_element['#type'])) {
            continue;
          }

          // Set composite title.
          if (isset($element['#title'])) {
            $composite_title = $this->t('@title - @cTitle (@key)', [
              '@title' => $element['#title'],
              '@cTitle' => $composite_element['#title'],
              '@key' => $element_key,
            ]);
          }
          else {
            $composite_title = "$element_key:$composite_key";
          }

          // Add phone element token.
          if ($composite_element['#type'] == 'tel') {
            $phoneElementOptions["[webform_submission:values:$element_key:$composite_key:raw]"] = $composite_title;
          }
        }
      }
    }

    $this->phoneElementOptions = $phoneElementOptions;
    $this->optionsElementOptions = $optionsElementOptions;
  }

  /**
   * Load available SMS templates.
   */
  private function loadTemplateOptions(): void {
    $templates = $this->templateService->getTemplates();
    $options = [];
    if (is_array($templates)) {
      foreach ($templates as $template) {
        $options[$template->getId()] = $template->getId();
      }
    }
    $this->smsTemplateOptions = $options;
  }

  /**
   * Get available SMS senders from SMSApi dashboard.
   */
  private function loadSenderOptions(): void {
    if (!$this->smsapiService->checkIfConnected()) {
      return;
    }
    $this->senderOptions = $this->smsapiService->getSenders();
  }

  /**
   * Generate template fields.
   *
   * @param string $template
   *   Template machine name.
   * @param array $options
   *   Default options.
   * @param array $form
   *   Form.
   */
  private function generateTemplateFields(string $template, array $options, array &$form): void {
    $template_entity = $this->templateService->getTemplate($template);
    if (!$template_entity instanceof SmsapiSmsTemplateInterface) {
      return;
    }
    $defaultTokens = $this->configuration['message']['sms_template']['tokens'] ?? [];

    // Hidden template name for future edit rendering.
    $form['template'] = [
      '#type' => 'hidden',
      '#value' => $template,
    ];

    $tokens = $template_entity->getTokens();
    $form['message']['sms_template']['sms_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#value' => $template_entity->getMessage(),
      '#disabled' => TRUE,
    ];
    foreach ($tokens as $token) {
      $tokenOptions = $options;
      $tokenOptions['custom'] = $this->t('Custom token value for @token', [
        '@token' => $token,
      ]);
      $form['message']['sms_template']['tokens'][$token]['type'] = [
        '#type' => 'select',
        '#title' => $this->t('Value for @token', [
          '@token' => $token,
        ]),
        '#options' => $tokenOptions,
        '#default_value' => $defaultTokens[$token]['type'] ?? '',
      ];
      $form['message']['sms_template']['tokens'][$token]['custom_token'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Custom value for token @token', [
          '@token' => $token,
        ]),
        '#default_value' => $defaultTokens[$token]['custom_token'] ?? '',
        '#states' => [
          'visible' => [
            ":input[name='settings[message][sms_template][tokens][{$token}][type]']" => ['value' => 'custom'],
          ],
        ],
      ];
    }
  }

  /**
   * Prepare Message data for SMS based on Webform submission data.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform
   *   Submitted Webform.
   * @param array $configuration
   *   Handler configuration.
   *
   * @return array
   *   Array of SMS Data for sending.
   */
  private function parseConfigData(WebformSubmissionInterface $webform, array $configuration): array {
    $configuration['to']['custom_number'] ??= '';
    /** @var string $number */
    $number = match($configuration['to']['to_number']) {
      'custom' => strip_tags($configuration['to']['custom_number']),
      default => $this->replaceTokens($configuration['to']['to_number'], $webform),
    };

    $number = explode(',', $number);
    $message = $this->prepareSmsMessage($configuration['message'], $webform);
    $configuration['from']['from_sender'] ??= 'test';

    return [
      'from' => strip_tags($configuration['from']['from_sender']),
      'to' => $number,
      'message' => $message,
    ];
  }

  /**
   * Prepares an SMS message.
   *
   * @param array $messageConfiguration
   *   The message configuration.
   * @param \Drupal\webform\WebformSubmissionInterface $webform
   *   The webform submission.
   *
   * @return array
   *   Returns the prepared message.
   */
  private function prepareSmsMessage(array $messageConfiguration, WebformSubmissionInterface $webform): array {
    $messageConfiguration['sms_template']['sms_message'] ??= '';
    $messageText = match(trim($messageConfiguration['message_type'])) {
      '_custom' => $this->replaceTokens(strip_tags($messageConfiguration['custom_message']), $webform),
      default => $messageConfiguration['sms_template']['sms_message']
    };

    $tokens = array_map(function ($data) use ($webform) {
      $data['custom_token'] ??= '';
      return match(trim($data['type'])) {
        'custom' => strip_tags($data['custom_token']),
        default => $this->replaceTokens($data['type'], $webform),
      };
    }, $messageConfiguration['sms_template']['tokens'] ?? []);

    return [
      'type' => $messageConfiguration['message_type'],
      'text' => $messageText,
      'tokens' => $tokens,
    ];
  }

  /**
   * Send SMS message.
   *
   * @param array $data
   *   SMS Data.
   */
  private function sendSms(array $data): void {
    $recipientsCount = count($data['to']);

    if ($recipientsCount === 0) {
      $this->loggerFactory->get('Webform SMSAPI')->error('Missing recipient number provided');
      return;
    }

    $isCustom = $data['message']['type'] === '_custom';

    // Determine which method to use and what arguments to send.
    if ($recipientsCount === 1) {
      if ($isCustom) {
        $this->smsapiService->sendSms($data['to'][0], $data['message']['text'], $data['from']);
      }
      else {
        $this->smsapiService->sendSmsWithTemplate($data['to'][0], $data['message']['type'], $data['message']['tokens'], $data['from']);
      }
    }
    else {
      if ($isCustom) {
        $this->smsapiService->sendMultipleSms($data['to'], $data['message']['text'], $data['from']);
      }
      else {
        $this->smsapiService->sendMultipleTemplateSms($data['to'], $data['message']['type'], $data['message']['tokens'], $data['from']);
      }
    }
    $this->getLogger('webform')
      ->notice('@form webform sent @title email.', [
        '@form' => $this->getWebform()->label(),
        '@title' => $this->label(),
        'link' => $this->getWebform()->toLink($this->t('Edit'), 'handlers')->toString(),
      ]);
  }

  /**
   * Trim webform token values.
   *
   * @param string $token
   *   Webform token.
   *
   * @return string
   *   Trimmed webform token.
   */
  private function trimWebformTokens(string $token): string {
    $patternsAndReplacements = [
      '/\[webform:([^:]+)\]/' => '[\1]',
      '/\[webform_role:([^:]+)\]/' => '[\1]',
      '/\[webform_access:type:([^:]+)\]/' => '[\1]',
      '/\[webform_group:role:([^:]+)\]/' => '[group:\1]',
      '/\[webform_group:owner:mail\]/' => '[group:owner]',
      '/\[webform_submission:(?:node|source_entity|values):([^]]+)\]/' => '[\1]',
      '/\[webform_submission:([^]]+)\]/' => '[\1]',
      '/(:raw|:value)(:html)?\]/' => ']',
    ];

    foreach ($patternsAndReplacements as $pattern => $replacement) {
      // PHPStan validation fix.
      $token = $token ?? '';
      $token = preg_replace($pattern, $replacement, $token);
    }
    return $token ?? '';
  }

}
