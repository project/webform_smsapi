# Webform SMSAPI

## Content of this file

* Introduction
* Requirements
* Installation
* Supporting organization

## Introduction

**Webform SMSAPI** provides the possibility of sending SMS messages using the [SMSAPI](https://www.drupal.org/project/smsapi) service by using a handler configured to suit your needs.
The module allows you to use pre-configured templates in the SMSAPI module or to send fully customized content.


## Requirements

* [SMSAPI](https://www.drupal.org/project/smsapi) - for handling sms sending request to SMSAPI service

## Installation

Install the Webform SMSAPI module as you would normally install a
contributed Drupal module. Visit [Installing Modules](https://www.drupal.org/node/1897420)
for further information.

## Configuration

Add a new *SMS Message* handler to an existing form and configure it according to your needs.

As part of the configuration, it is possible to fill in the recipient and the content of the SMS message using the values from the fields filled in during the webform submission, or to define these parameters using your own values.

## Supporting organization:

* [Smartbees](https://www.drupal.org/smartbees)
